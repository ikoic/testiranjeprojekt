import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FirstTestDemoShop {

    public WebDriver driver;

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\inesk\\IdeaProjects\\chromedriver.exe");

        driver = new ChromeDriver();

        driver.navigate().to("https://www.google.com/");
    }

    @Test
    public void demoPage() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        driver.quit();
    }

    @Test
    public void demoRegistration() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        driver.findElement(By.className("ico-register")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("gender-female")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("FirstName")).sendKeys("test911");
        Thread.sleep(2000);
        driver.findElement(By.id("LastName")).sendKeys("test92");
        driver.findElement(By.id("Email")).sendKeys("test01@gmail.com");
        Thread.sleep(2000);
        driver.findElement(By.id("Password")).sendKeys("12345test1");
        Thread.sleep(2000);
        driver.findElement(By.id("ConfirmPassword")).sendKeys("12345test1");
        Thread.sleep(2000);
        driver.findElement(By.id("register-button")).click();
        driver.findElement(By.className("button-1 register-continue-button")).click();
        driver.quit();
    }

    @Test
    public void wrongConfirmPassword() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        driver.findElement(By.className("ico-register")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("gender-female")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("FirstName")).sendKeys("test9");
        Thread.sleep(2000);
        driver.findElement(By.id("LastName")).sendKeys("test90");
        driver.findElement(By.id("Email")).sendKeys("test00@gmail.com");
        Thread.sleep(2000);
        driver.findElement(By.id("Password")).sendKeys("123test1");
        Thread.sleep(2000);
        driver.findElement(By.id("ConfirmPassword")).sendKeys("345675test123");
        Thread.sleep(2000);
        driver.quit();
    }

    @Test
    public void AddInShoppingCart() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[4]/div[3]/div/div/div[3]/div[3]/div/div[2]/div[3]/div[2]/input")).click();
        Thread.sleep(5000);
        driver.findElement((By.xpath("//*[@id=\"topcartlink\"]/a"))).click();
        Thread.sleep(5000);
        driver.quit();

    }

    @Test
    public void DeleteInShoppingCart() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[4]/div[3]/div/div/div[3]/div[3]/div/div[2]/div[3]/div[2]/input")).click();
        Thread.sleep(5000);
        driver.findElement((By.xpath("//*[@id=\"topcartlink\"]/a"))).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[4]/div/div/div[2]/div/form/table/tbody/tr/td[1]/input")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[4]/div/div/div[2]/div/form/div[1]/div/input[1]")).click();
        Thread.sleep(5000);
        driver.quit();


    }

    @Test
    public void SearchLaptopandAddInShoppingCart() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        driver.findElement(By.id("small-searchterms")).sendKeys("laptop");
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[1]/div[3]/form/input[2]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[4]/div[2]/div/div[2]/div[3]/div[1]/div/div/div[2]/div[3]/div[2]/input")).click();
        Thread.sleep(2000);
        driver.quit();

    }

    @Test
    public void SearchWrong() throws InterruptedException {
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Demo Web Shop");
        searchTextBox.submit();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a[1]/h3")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("small-searchterms")).sendKeys("shdddd");
        Thread.sleep(10000);
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[1]/div[3]/form/input[2]")).click();
        Thread.sleep(2000);
        driver.quit();

    }


}